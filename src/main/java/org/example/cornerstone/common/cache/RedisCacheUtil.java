package org.example.cornerstone.common.cache;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundSetOperations;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

public class RedisCacheUtil {

    private final RedisTemplate redisTemplate;

    @Autowired
    public RedisCacheUtil(RedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    /**
     * 不考虑过期时间以及存储value的类型
     *
     * @param key   key
     * @param value value
     */
    public <T> void setCacheObject(final String key, final T value) {
        redisTemplate.opsForValue().set(key, value);
    }

    /**
     * 多少单位时间后过期
     *
     * @param key      key
     * @param value    value
     * @param timeout  过期时间长度
     * @param timeUnit 单位
     */
    public <T> void setCacheObject(final String key, final T value, final Integer timeout,
                                   final TimeUnit timeUnit) {
        redisTemplate.opsForValue().set(key, value, timeout, timeUnit);
    }

    /**
     * @param key      key
     * @param dataList dataList
     * @return
     */
    public <T> long setCacheList(final String key, final List<T> dataList) {
        Long count = redisTemplate.opsForList().rightPushAll(key, dataList);
        return count == null ? 0 : count;
    }

    /**
     * @param key     key
     * @param dataSet dataSet
     * @return
     */
    public <T> BoundSetOperations<String, T> setCacheSet(final String key, final Set<T> dataSet) {

        final BoundSetOperations<String, T> setOperation = redisTemplate.boundSetOps(key);
        for (final T t : dataSet) {
            setOperation.add(t);
        }
        return setOperation;
    }

    public <T> void setCacheMap(final String key, final Map<String, T> dataMap) {
//        if (!MapUtils.isEmpty(dataMap)) {
//            redisTemplate.opsForHash().putAll(key, dataMap);
//        }
    }

    public <T> void setCacheMapValue(final String key, final String hashKey, final T value) {
        redisTemplate.opsForHash().put(key, hashKey, value);
    }

    /**
     * 修改某个key的过期时间
     *
     * @param key     key
     * @param timeout timeout
     * @param unit    unit
     * @return
     */
    public Boolean expire(final String key, final long timeout, final TimeUnit unit) {
        return redisTemplate.expire(key, timeout, unit);
    }

    /**
     * 修改过期时间为指定的时间点
     *
     * @param key      key
     * @param dateTime timeout
     * @return
     */
    public Boolean expireAt(final String key, final Date dateTime) {
        return redisTemplate.expireAt(key, dateTime);
    }

    /**
     * redisTemplate提供的expireAt方法入参是Date类，这个类是不包含时区信息的，
     * 完全按照redis服务器的默认时区来算时间，所以在插入前需要转为对应时区的时间
     *
     * @param key       key
     * @param localDate timeout
     * @return
     */
    public boolean expireAt(final String key, final LocalDate localDate) {
//        return redisTemplate.expireAt(key, LocalDateUtil.convertToDate(localDate));
        return false;
    }

    /**
     * 获取还有多久过期，单位默认为毫秒
     *
     * @param key key
     * @return long TimeUnit: milliseconds
     */
    public Long getExpire(final String key) {
        return getExpire(key, TimeUnit.MILLISECONDS);
    }

    /**
     * 获取还有多久过期，单位可做入参
     *
     * @param key  key
     * @param unit TimeUnit
     * @return long TimeUnit: milliseconds
     */
    public Long getExpire(final String key, final TimeUnit unit) {
        return redisTemplate.getExpire(key, unit);
    }

    public <T> T getCacheObject(final String key) {
        final ValueOperations<String, T> operation = redisTemplate.opsForValue();
        return operation.get(key);
    }

    public <T> List<T> getCacheList(final String key) {
        return redisTemplate.opsForList().range(key, 0, -1);
    }

    /**
     * @param key key
     * @return
     */
    public <T> Set<T> getCacheSet(final String key) {
        return redisTemplate.opsForSet().members(key);
    }

    /**
     * @param key key
     * @return
     */
    public <T> Map<String, T> getCacheMap(final String key) {
        return redisTemplate.opsForHash().entries(key);
    }

    public <T> T getCacheMapValue(final String key, final String hashKey) {
        HashOperations<String, String, T> opsForHash = redisTemplate.opsForHash();
        return opsForHash.get(key, hashKey);
    }

    public void delCacheMapValue(final String key, final String hashKey) {
        redisTemplate.opsForHash().delete(key, hashKey);
    }

    public <T> List<T> getMultiCacheMapValue(final String key, final Collection<Object> hashKey) {
        return redisTemplate.opsForHash().multiGet(key, hashKey);
    }

    public Collection<String> keys(final String pattern) {
        return redisTemplate.keys(pattern);
    }

    /**
     * @param key key
     */
    public Boolean deleteObject(final String key) {
        return redisTemplate.delete(key);
    }

    /**
     * @param collection collection
     * @return
     */
    public long deleteObject(final Collection collection) {
        return redisTemplate.delete(collection);
    }


}
