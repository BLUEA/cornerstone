package org.example.cornerstone.common.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties("cornerstone")
public class CornerstoneProperties {

    private String value;
}
