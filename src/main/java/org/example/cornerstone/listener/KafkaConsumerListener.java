package org.example.cornerstone.listener;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KafkaConsumerListener {

    @KafkaListener(topics = "test", groupId = "test-group")
    public void listen(String message) {
        // 处理接收到的消息
        System.out.println("Received message: " + message);
        // 在这里可以编写处理接收到消息的逻辑
    }
}
