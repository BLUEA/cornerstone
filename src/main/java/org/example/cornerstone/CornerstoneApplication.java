package org.example.cornerstone;

import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.annotation.MapperScans;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("org.example.cornerstone.dao")
@SpringBootApplication
public class CornerstoneApplication {
    public static void main(String[] args) {
        SpringApplication.run(CornerstoneApplication.class, args);
    }
}
