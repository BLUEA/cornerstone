package org.example.cornerstone.util;

import java.util.Optional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

@Slf4j
public class JsonUtil {

    private static final ObjectMapper objectMapper;

    static {
        objectMapper = new ObjectMapper();
    }

    public static ObjectMapper getObjectMapper() {
        return objectMapper;
    }

    public static <T> String toJson(final T objBean) {
        return Optional.ofNullable(objBean).map(obj -> {
            try {
                return obj instanceof String ? obj.toString() :
                        objectMapper.writeValueAsString(obj);
            } catch (JsonProcessingException e) {
                log.error("Failed to parse object to JSON string", e);
                return "";
            }
        }).orElse("");
    }

    public static <T> T toBean(final String jsonString, final Class<T> clazz) {
        if (StringUtils.isBlank(jsonString) || clazz == null) {
            return null;
        }
        try {
            return objectMapper.readValue(jsonString, clazz);
        } catch (Exception e) {
            log.error("Parse String to Bean error", e);
        }
        return null;
    }

}
