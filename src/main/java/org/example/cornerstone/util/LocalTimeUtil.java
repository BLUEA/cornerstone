package org.example.cornerstone.util;

import java.time.format.DateTimeFormatter;

public class LocalTimeUtil {

    public static final DateTimeFormatter FORMAT_TIME = DateTimeFormatter.ofPattern("HH:mm:ss");

}
