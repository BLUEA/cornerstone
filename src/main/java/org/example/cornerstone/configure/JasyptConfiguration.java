package org.example.cornerstone.configure;

import org.jasypt.encryption.pbe.PooledPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.SimpleStringPBEConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JasyptConfiguration {

    private static final String POOL_SIZE = "1";
    private static final String ITERATIONS = "10000";
    private static final String PROVIDER_NAME = "SunJCE";
    private static final String OUTPUT_TYPE = "base64";
    private static final String PASSWORD = "org#example%cornerstone$0128";
    private static final String ALGORITHM = "PBEWITHHMACSHA512ANDAES_256";
    private static final String IV_GENERATOR_CLASSNAME = "org.jasypt.iv.RandomIvGenerator";
    private static final String SALT_GENERATOR_CLASSNAME = "org.jasypt.salt.RandomSaltGenerator";

    @Bean("jasyptStringEncryptor")
    public PooledPBEStringEncryptor jasyptEncryptor() {
        final PooledPBEStringEncryptor encryptor = new PooledPBEStringEncryptor();
        final SimpleStringPBEConfig config = new SimpleStringPBEConfig();
        config.setProviderName(PROVIDER_NAME);
        config.setAlgorithm(ALGORITHM);
        config.setPassword(PASSWORD);
        config.setPoolSize(POOL_SIZE);
        config.setKeyObtentionIterations(ITERATIONS);
        config.setStringOutputType(OUTPUT_TYPE);
        config.setSaltGeneratorClassName(SALT_GENERATOR_CLASSNAME);
        config.setIvGeneratorClassName(IV_GENERATOR_CLASSNAME);
        encryptor.setConfig(config);

        return encryptor;
    }
}
