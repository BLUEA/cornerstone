package org.example.cornerstone.exception;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

@Setter
@Getter
public class ApplicationException extends RuntimeException {

    private Integer code;

    ApplicationException() {
        super();
    }

    ApplicationException(String message, Throwable cause) {
        super(StringUtils.defaultIfBlank(message, StringUtils.EMPTY), cause);
    }

    ApplicationException(Integer code, String message) {
        super(StringUtils.defaultIfBlank(message, StringUtils.EMPTY));
        this.code = code;
    }

    ApplicationException(Integer code, String message, Throwable cause) {
        this(message, cause);
        this.code = code;
    }

}
