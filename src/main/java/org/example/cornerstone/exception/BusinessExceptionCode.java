package org.example.cornerstone.exception;

public enum BusinessExceptionCode implements ErrorWrapper {

    SOME_BUSINESS_ERROR(10000, "something business error");

    private Integer code;
    private String message;

    BusinessExceptionCode(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public Integer getCode() {
        return code;
    }

    public static BusinessException reException(Integer code, String message) {
        return new BusinessException(code, message);
    }

    public static BusinessException reException(BusinessExceptionCode exceptionEnum) {
        return new BusinessException(exceptionEnum.code, exceptionEnum.message);
    }

    public static BusinessException reException(BusinessExceptionCode exceptionEnum,
                                                Throwable cause) {
        return new BusinessException(exceptionEnum.code, exceptionEnum.message, cause);
    }
}
