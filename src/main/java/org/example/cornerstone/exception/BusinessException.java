package org.example.cornerstone.exception;

public class BusinessException extends ApplicationException {

    BusinessException() {
        super();
    }

    BusinessException(Integer code, String message) {
        super(code, message);
    }

    BusinessException(Integer code, String message, Throwable cause) {
        super(code, message, cause);
    }

    BusinessException(String message, Throwable cause) {
        super(message, cause);
    }

}
