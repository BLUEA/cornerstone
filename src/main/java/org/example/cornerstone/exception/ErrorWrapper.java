package org.example.cornerstone.exception;

import java.text.MessageFormat;
import java.util.Objects;

public interface ErrorWrapper {

    Object[] defaultMessage = new Object[] {"", "", "", ""};

    String getMessage();

    Integer getCode();

    default ApplicationException exception(Object... args) {
        return new ApplicationException(getCode(), MessageFormat.format(getMessage(),
                Objects.nonNull(args) && args.length > 0 ? args :defaultMessage));
    }
}
