package org.example.cornerstone.controller;

import lombok.RequiredArgsConstructor;
import org.example.cornerstone.common.properties.CornerstoneProperties;
import org.example.cornerstone.pojo.dto.ResponseDto;
import org.example.cornerstone.pojo.entity.UserInfo;
import org.example.cornerstone.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class TestController {

    private final TestService testService;

    @Value("${cornerstone.value}")
    private String testValue;

    private final CornerstoneProperties cornerstoneProperties;

    @GetMapping("/health")
    public ResponseDto<String> health() {
        return ResponseDto.success("hello");
    }

    @GetMapping("/mybatis")
    public UserInfo testMybatis() {
        return testService.testMybatis("bluea", "123456");
    }

    @GetMapping("/kafka")
    public void testKafka(@RequestParam("msg") String msg) {
        testService.sendMessage("test", msg);
    }

    @GetMapping("/redis")
    public void testRedis(@RequestParam("value") String value) {
        System.out.println(testValue);
        System.out.println(cornerstoneProperties.getValue());
        testService.redisSaveTest(value);
    }
}
