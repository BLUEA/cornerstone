package org.example.cornerstone.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.example.cornerstone.pojo.entity.UserInfo;

@Mapper
public interface UserInfoDao {
    UserInfo findByUsernameAndPassword(@Param("username") String username, @Param("password") String password);
}
