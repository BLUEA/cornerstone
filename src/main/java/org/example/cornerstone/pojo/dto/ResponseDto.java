package org.example.cornerstone.pojo.dto;

import java.io.Serializable;
import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class ResponseDto<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer code;
    private String msg;
    private T data;

    private static final Integer SUCCESS_CODE = 200;
    private static final String SUCCESS_MSG = "success";

    private static final String FAIL_MSG = "fail";

    public static <T> ResponseDto<T> success() {
        ResponseDto<T> responseDto = new ResponseDto<>();
        responseDto.code = SUCCESS_CODE;
        responseDto.msg = SUCCESS_MSG;
        return responseDto;
    }

    public static <T> ResponseDto<T> success(T data) {
        ResponseDto<T> responseDto = new ResponseDto<>();
        responseDto.code = SUCCESS_CODE;
        responseDto.msg = SUCCESS_MSG;
        responseDto.data = data;
        return responseDto;
    }

    public static <T> ResponseDto<T> success(T data, String msg) {
        ResponseDto<T> responseDto = new ResponseDto<>();
        responseDto.code = SUCCESS_CODE;
        responseDto.msg = Optional.ofNullable(msg).map(String::trim).orElse(SUCCESS_MSG);
        responseDto.data = data;
        return responseDto;
    }

    public static <T> ResponseDto<T> fail(Integer code, String msg) {
        ResponseDto<T> responseDto = new ResponseDto<>();
        responseDto.code = code;
        responseDto.msg = msg;
        return responseDto;
    }

    public static <T> ResponseDto<T> newInstance(Integer code, String msg, T data) {
        ResponseDto<T> responseDto = new ResponseDto<>();
        responseDto.code = code;
        responseDto.msg = msg;
        responseDto.data = data;
        return responseDto;
    }

    @JsonIgnore
    public boolean isSuccess() {
        return SUCCESS_CODE.equals(this.code);
    }

    @JsonIgnore
    public boolean isFail() {
        return !SUCCESS_CODE.equals(this.code);
    }

}
