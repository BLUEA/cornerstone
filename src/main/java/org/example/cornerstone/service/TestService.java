package org.example.cornerstone.service;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import lombok.RequiredArgsConstructor;
import org.example.cornerstone.common.cache.RedisCacheUtil;
import org.example.cornerstone.dao.UserInfoDao;
import org.example.cornerstone.exception.BusinessExceptionCode;
import org.example.cornerstone.pojo.entity.UserInfo;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TestService {

    private final UserInfoDao userInfoDao;

    private final KafkaTemplate<String, String> kafkaTemplate;

    private final RedisCacheUtil redisCacheUtil;

    public UserInfo testMybatis(String username, String password) {
        return userInfoDao.findByUsernameAndPassword(username, password);
    }

    public void sendMessage(String topic, String message) {
        kafkaTemplate.send(topic, message);
    }

    public void redisSaveTest(String value) {
        try {
            redisCacheUtil.setCacheObject("testKey", value);
            redisCacheUtil.setCacheObject("testKeyTimeOut", value, 3000, TimeUnit.SECONDS);
            Date date = new Date(124, 2, 11, 18, 0, 0);
            redisCacheUtil.expireAt("testKey", date);
        } catch (RuntimeException e) {
            throw BusinessExceptionCode.reException(BusinessExceptionCode.SOME_BUSINESS_ERROR, e);
        }
    }
}
