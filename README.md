# cornerstone

### 介绍

准备好开发过程中所需要的基本插件，可用于直接开发具体业务

### 软件架构

版本:
- JDK: 11
- Spring Boot: 2.7.11
- MySQL: 8.0.32
- Mybatis: 2.2.0
- Redis: 2.7.11
- Kafka: 2.8.11

### 使用说明
本项目准备了开发所需要的基本组件，持久化有MySQL，缓存有Redis/Cache，异步可以使用消息队列Kafka/线程池

#### 缓存的使用
构建缓存的代码在`org.example.cornerstone.common.cache`中，结构如图：

![缓存框架图](README-IMG/cache-structure.png)

大部分的开发任务都可以用RedisCacheUtil工具类解决，AbstractCache的目的是为了应对多种缓存方式而做出来的，
这里加多一个本地缓存，以及优先级缓存，规范Redis的缓存进AbstractCache是为了能够交给缓存管理器来管理过期时间；

如果说是一个单体项目，最多就是多服务器的集群部署，这时候redis是完全够用。如果并不需要管理这些，
可以只用RedisCacheUtil工具类，其他的删掉即可。

#### 异常的使用
业务异常在后面自己定制异常码和异常信息。

抛出异常写法：
```java
throw BusinessExceptionCode.reException(BusinessExceptionCode.SOME_BUSINESS_ERROR, e);
```

### 更新日志
#### 2024.3.15
- 完善了整个缓存的框架，以spring framework的Cache接口作为基础，抽象出了本地和Redis两种缓存方式， 以及一个优先级缓存。
- 统一相应格式ResponseDto

#### 2024.3.18
- 搭建异常处理的框架，准备了业务异常专用的BusinessException和框架专用的FrameworkException异常。
