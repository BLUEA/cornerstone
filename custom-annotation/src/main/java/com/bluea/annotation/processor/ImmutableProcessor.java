package com.bluea.annotation.processor;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Set;
import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;
import javax.tools.JavaFileObject;

import com.bluea.annotation.Immutable;

@SupportedAnnotationTypes("com.bluea.annotation.Immutable")
@SupportedSourceVersion(SourceVersion.RELEASE_11)
public class ImmutableProcessor extends AbstractProcessor {

    private Elements elementUtils;

    @Override
    public synchronized void init(ProcessingEnvironment processingEnv) {
        super.init(processingEnv);
        elementUtils = processingEnv.getElementUtils();
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        for (Element annotatedElement : roundEnv.getElementsAnnotatedWith(
                Immutable.class)) {
            if (annotatedElement.getKind() == ElementKind.CLASS) {
                TypeElement typeElement = (TypeElement) annotatedElement;
                try {
                    generateImmutableClass(typeElement);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return true;
    }

    private void generateImmutableClass(TypeElement typeElement) throws IOException {
        String packageName = elementUtils.getPackageOf(typeElement).getQualifiedName().toString();
        String className = typeElement.getSimpleName().toString() + "Immutable";
        String fullName = packageName + "." + className;

        JavaFileObject fileObject = processingEnv.getFiler().createSourceFile(fullName);
        try (PrintWriter writer = new PrintWriter(fileObject.openWriter())) {
            writer.println("package " + packageName + ";");
            writer.println();
            writer.println("public final class " + className + " {");
            for (Element element : typeElement.getEnclosedElements()) {
                if (element.getKind() == ElementKind.FIELD) {
                    String fieldName = element.getSimpleName().toString();
                    String fieldType = getLastTypeName(element.asType().toString());
                    writer.println("    private " + fieldType + " " + fieldName + ";");
                }
            }
            writer.println();
            writer.println(
                    "    public " + className + "(" + typeElement.getSimpleName() + " " +
                            lowercase(typeElement.getSimpleName().toString()) + ") {");
            for (Element element : typeElement.getEnclosedElements()) {
                if (element.getKind() == ElementKind.FIELD) {
                    String fieldName = element.getSimpleName().toString();
                    writer.println("        this." + fieldName + " = " +
                            lowercase(typeElement.getSimpleName().toString()) + "." + fieldName +
                            ";");
                }
            }
            writer.println("    }");
            writer.println();
            for (Element element : typeElement.getEnclosedElements()) {
                if (element.getKind() == ElementKind.FIELD) {
                    String fieldName = element.getSimpleName().toString();
                    String fieldType = getLastTypeName(element.asType().toString());
                    writer.println(
                            "    public " + fieldType + " get" + capitalize(fieldName) + "() {");
                    writer.println("        return " + fieldName + ";");
                    writer.println("    }");
                    writer.println();
                    writer.println(
                            "    public void set" + capitalize(fieldName) + "(" + fieldType + " " +
                                    fieldName + ") {");
                    writer.println("        this." + fieldName + " = " + fieldName + ";");
                    writer.println("    }");
                    writer.println();
                }
            }
            writer.println("}");
        }
    }

    private String capitalize(String s) {
        return Character.toUpperCase(s.charAt(0)) + s.substring(1);
    }

    private String lowercase(String s) {
        return Character.toLowerCase(s.charAt(0)) + s.substring(1);
    }

    private String getLastTypeName(String s) {
        String[] split = s.split("/.");
        return split[split.length - 1];
    }
}
